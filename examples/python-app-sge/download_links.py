import json, requests
import os, traceback
from bs4 import BeautifulSoup

def download_book_from_itebooks(json_file):
	o = open(json_file, 'r')
	json_obj = json.load(o)

	for i in json_obj:
		r = requests.get(i['url'])
		s = BeautifulSoup(r.text, 'html.parser')
		#import pdb; pdb.set_trace()
		download_link = s.find_all('span', class_='download-links')[0].a.get('href')
		filename = 'downloads/%s.pdf' % i['title']
		r2 = requests.get(download_link)
		with open(filename, 'wb') as download_file:
			download_file.write(r2.content)
